import React, { Component } from 'react';
import ButtonAppBar from './components/header';
import MainAppBarHeader from './components/mainHeader';
import MainAppBarBody from './components/mainBody';
import FooterAppBar from './components/footer';
import DialogsBar from './components/dialogs';
import './App.css';
import './assets/css/styles.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ButtonAppBar />
        <MainAppBarHeader />
        <MainAppBarBody />
        <FooterAppBar />
        <DialogsBar />
      </div>
    );
  }
}

export default App;
