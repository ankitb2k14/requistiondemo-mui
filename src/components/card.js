import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

const styles = theme => ({
    card: {
        display: 'flex',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    content: {
        flex: '1 0 auto',
        color: 'rgba(0, 0, 0, 0.86)',
        padding: '26px  53px 32px 0',
        marginLeft: '21px'
    },
    cardHeader: {
        backgroundColor: '#e1edf9',
        width: '100%',
        height: '88px',
        display: 'flex',
        flexDirection: 'row'
    },
    CardContent: {
        width: 'auto',
        height: '100%'
    },
    cover: {
        width: 39,
        height: 45,
        margin: '20px 0 23px 26px'
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
    },
    playIcon: {
        height: 38,
        width: 38,
    },
    candidate: {
        fontWeight: 'bold',
        fontSize: '18px',
        color: 'rgba(0, 0, 0, 0.87)',
        'line-height': '1.39'
    },
    listText: {
        fontSize: '18px',
        color: 'rgba(0, 0, 0, 0.87)'
    }
});

class MediaCard extends Component {

    render() {
        const { classes, toggleApplicantPool } = this.props;
        return (
            toggleApplicantPool ? <Card className={classes.card}>
                <div className={classes.details}>
                    <div className={classes.cardHeader}>
                        <CardMedia style={{ backgroundSize: 'contain' }}
                            className={classes.cover}
                            image="/images/multicolored-spark.png"
                            title="Multicolor Spark"
                        />
                        <CardContent className={classes.content + " commonClass"}>
                            <Typography style={{ fontSize: '22px' }}>Applicant Pool (20)</Typography>
                        </CardContent>
                    </div>
                    <CardContent className={classes.CardContent}>
                        <List component="nav" className="first-list">
                            <ListItem style={{ marginTop: '50px' }}>
                                <Typography variant="title" className={classes.candidate}>
                                    Internal Candidates:
                                </Typography>
                                <ListItemSecondaryAction>
                                    <Typography variant="title" className={classes.candidate}>
                                        10
                                    </Typography>
                                </ListItemSecondaryAction>
                            </ListItem>
                            <ListItem style={{ marginTop: '15px' }}>
                                <ListItemText primary="Excellent Fit:" />
                                <Typography variant="title" className={classes.listText}>
                                    5
                                    </Typography>
                            </ListItem>
                            <ListItem style={{ marginTop: '15px', marginBottom: '30px' }}>
                                <ListItemText primary="Good Fit:" />
                                <Typography variant="title" className={classes.listText}>
                                    5
                                </Typography>
                            </ListItem>
                        </List>
                        <List component="nav">
                            <ListItem style={{ marginTop: '25px' }}>
                                <Typography variant="title" className={classes.candidate}>
                                    External Candidates:
                                </Typography>
                                <ListItemSecondaryAction>
                                    <Typography variant="title" className={classes.candidate}>
                                        10
                                    </Typography>
                                </ListItemSecondaryAction>
                            </ListItem>
                            <ListItem style={{ marginTop: '15px' }}>
                                <ListItemText primary="Excellent Fit:" />
                                <Typography variant="title" className={classes.listText}>
                                    7
                                    </Typography>
                            </ListItem>
                            <ListItem style={{ marginTop: '15px' }}>
                                <ListItemText primary="Good Fit:" />
                                <Typography variant="title" className={classes.listText}>
                                    7
                                    </Typography>
                            </ListItem>
                        </List>
                    </CardContent>
                </div>

            </Card> : ''
        );
    }

}

MediaCard.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(MediaCard);