import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    headerTitle: {
        fontSize: '18px',
        color: 'rgba(0, 0, 0, 0.86)',
        paddingTop: '8px',
        paddingLeft: '10px'  
    },
    dashboard: {
        fontSize: '18px',
        color: '#0071ce',
        float: 'right',
        paddingRight: '22px'
    }
});

function MainAppBar(props) {
    const { classes } = props;
    return (
        <footer id="footer-wrapper" className={classes.root}>
            <Grid container spacing={24} style={{ padding: '0  24px' }}>
                <Grid item xs={6}>
                    <Typography className={classes.headerTitle + ' commonClass'}>
                        &copy; 2018 Walmart
                    </Typography>
                </Grid>
                <Grid item xs={6}>
                    <Typography variant="title" color="inherit" className={classes.dashboard}>
                        <img className="img-responsive" src="images/wmt-sams-logo.png" alt="footer-logo" />
                    </Typography>
                </Grid>
            </Grid>
        </footer>
    );
}
MainAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(MainAppBar);
