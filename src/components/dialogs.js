import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Icon from '@material-ui/core/Icon';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import '../css/dialog.css'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  cardHeader: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '9px',
    marginLeft: '20px',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  cover: {
    width: 18,
    height: 18
  },
  content: {
    flex: '1 0 auto',
    color: 'rgba(0, 0, 0, 0.86)',
    padding: '0',
    marginLeft: '9px'
  },
  dialogHeader: {
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      height: '58px',
      backgroundColor: '#0f1c2c',
      boxShadow: '0 0 4px 0 rgba(0, 0, 0, 0.12)'
    }
  },
  dialogContent: {
    [theme.breakpoints.down('sm')]: {
      flex: 'none'
    }
  },
  dialogTitle: {
    fontSize: '18px',
    width: '150px',
    height: '25px',
    color: '#333333',
    marginTop: '28px',
    marginLeft: '44px',
    [theme.breakpoints.down('sm')]: {
      margin: '16px auto 17px',
      color: '#ffffff'
    }
  },
  gridContainer: {
    paddingLeft: '20px',
    [theme.breakpoints.down('sm')]: {
      marginTop: '37px',
      paddingLeft: '15px'
    }
  },
  dialogCaption: {
    fontSize: '14px',
    color: 'rgba(0, 0, 0, 0.54)',
    lineHeight: '1.07'
  },
  dialogBody: {
    fontSize: '14px',
    color: 'rgba(0, 0, 0, 0.87)'
  },
  dialogNote: {
    color: 'rgba(0, 0, 0, 0.87)',
    margin: '33px 0 25px 20px',
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  dashboard: {
    width: '200px',
    height: '46px',
    fontSize: '18px',
    [theme.breakpoints.down('sm')]: {
      width: '154px',
      height: '46px'
    }
  },
  dashboard1: {
    width: '200px',
    height: '46px',
    fontSize: '18px',
    marginLeft: '47px',
    [theme.breakpoints.down('sm')]: {
      width: '154px',
      height: '46px',
      marginLeft: '36px',
      marginRight: '18px'
    }
  },
  sectionMobile: {
    display: 'block',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    }
  }
});


class DialogsBar extends React.Component {
  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, fullScreen } = this.props;

    return (
      <div>
        <Button onClick={this.handleClickOpen}>Open responsive dialog</Button>
        <Dialog
          fullScreen={fullScreen}
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <div className={classes.dialogHeader}>
            <Typography className={classes.dialogTitle + " commonClass"}>
              Requistions Review
            </Typography>
          </div>
          <DialogContent className={classes.dialogContent}>
            <div className={classes.root}>
              <div className={classes.cardHeader}>
                <CardMedia style={{ backgroundSize: 'contain' }}
                  className={classes.cover}
                  image="/images/information-circled-copy.png"
                  title="Info"
                />
                <CardContent className={classes.content + " commonClass"}>
                  <Typography className="commonClass" style={{ color: '#000' }}>
                    New requisitions will be closed 30 days after creation date.</Typography>
                </CardContent>
              </div>
              <Grid container spacing={24} className={classes.gridContainer}>
                <Grid item lg={6} md={6} sm={6} xs={5} >
                  <Typography title="caption" className={classes.dialogCaption + " commonClass"}>
                    Employment Type
                   </Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={7}>
                  <Typography title="body1" className={classes.dialogBody + " commonClass"}>
                    Full Time
                    </Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={5} >
                  <Typography title="caption" className={classes.dialogCaption + " commonClass"}>
                    No. of Positions
                   </Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={7}>
                  <Typography title="body1" className={classes.dialogBody + " commonClass"}>
                    6-10-101 [DEPARTMENT MANAGER]
                    </Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={5} >
                  <Typography title="caption" className={classes.dialogCaption + " commonClass"}>
                    Hiring Manager
                   </Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={7}>
                  <Typography title="body1" className={classes.dialogBody + " commonClass"}>
                    CIMILLUCA CHITTY
                    </Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={5}>
                  <Typography title="caption" className={classes.dialogCaption + " commonClass"}>
                    Reason
                   </Typography>
                </Grid>
                <Grid item lg={6} md={6} sm={6} xs={7}>
                  <Typography title="body1" className={classes.dialogBody + " commonClass"}>
                    Different shift selection needed
                    </Typography>
                </Grid>
              </Grid>
              <Typography className={classes.dialogNote + " commonClass"}>
                Do you want to continue? Click on ‘OK’ to create requisition, otherwise click on ‘Cancel’.
                </Typography>
            </div>
          </DialogContent>
          <DialogActions>
            <div className="dialog-footer">
              <Button className={classes.dashboard + " commonClass dialog-cancel"} color="inherit" >
                Cancel
                                </Button>
              <Button className={classes.dashboard1 + " commonClass dialog-submit"} color="inherit" >
                Submit
                                </Button>
            </div>
          </DialogActions>
        </Dialog>
      </div >
    );
  }
}

DialogsBar.propTypes = {
  fullScreen: PropTypes.bool.isRequired,
};

export default withMobileDialog()(withStyles(styles)(DialogsBar));
