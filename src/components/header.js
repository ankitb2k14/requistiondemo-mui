import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menubar: {
        backgroundColor: '#041f41'
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    button : {
        textDecoration: 'underline',
        textTransform: 'capitalize',
        fontSize: '16px'
    }
};

function ButtonAppBar(props) {
    const { classes } = props;
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar className={classes.menubar}>
                    <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                        <MenuIcon style={{ fontSize: '1.6em' }} />
                    </IconButton>
                    <Typography variant="title" color="inherit" className={classes.grow}>
                        <img src="images/logo-copy.png" alt="logo" />
                    </Typography>
                    <Button className={classes.button + " commonClass"} color="inherit" >
                        Log Out
                     </Button>
                </Toolbar>
            </AppBar>
        </div >
    );
}

ButtonAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonAppBar);
