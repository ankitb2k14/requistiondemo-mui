import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import MediaCard from './card';
import '../css/mainBody.css';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    headerTitle: {
        fontSize: '18px',
        color: 'rgba(0, 0, 0, 0.86)',
        [theme.breakpoints.down('sm')]: {
            marginBottom: '26px'
        }
    },
    headerTitle1: {
        fontSize: '18px',
        color: 'rgba(0, 0, 0, 0.86)',
        [theme.breakpoints.down('sm')]: {
            marginTop: '62px',
            marginBottom: '42px'
        }
    },
    textField: {
        opacity: 1,
        color: '#000000'
    },
    labelText: {
        fontSize: '12px',
        color: '#4a4a4a'
    },
    formControl: {
        minWidth: 120,
    },
    potentialButton: {
        display: 'block',
        color: '#0071ce',
        fontSize: '16px',
        textTransform: 'capitalize',
        textDecoration: 'underline',
        padding: '0'
    },
    dashboard: {
        fontSize: '18px',
        color: '#0071ce',
        float: 'right'
    },
    infoSelect: {
        fontSize: '14px',
        color: '#d0021b',
        marginTop: '4px'
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'block'
        },
    },
    sectionMobile: {
        display: 'block',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    gridContainer: {
        marginBottom: '77px',
        [theme.breakpoints.down('sm')]: {
            marginBottom: '46px'
        }
    },
    selectForm: {
        [theme.breakpoints.down('sm')]: {
            marginBottom: '15px'
        }
    },
    cardHeader: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: '30px'
    },
    CardContent: {
        width: 'auto',
        height: '100%'
    },
    cover: {
        width: 18,
        height: 18
    },
});

class MainAppBarBody extends Component {

    constructor(props) {
        super(props);

        this.state = {
            toggleApplicantPool: false
        }
    }
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root + ' main-wrapper'} style={{ padding: '0  24px' }}>
                <Grid container className={classes.root} spacing={24}>
                    <Grid item lg={9} md={8} sm={12} xs={12} style={{ paddingRight: '15.4px' }}>
                        <Grid container className={classes.root} spacing={24}>
                            <Grid item lg={4} md={12} sm={12} xs={12} style={{ paddingRight: '15.4px' }}>
                                <Typography
                                    variant="display2" gutterBottom className={classes.headerTitle + ' commonClass'}>
                                    Search or Enter Job Code
                                    </Typography>
                                <TextField
                                    id="standard-name"
                                    label="Job Title"
                                    placeholder="Start typing job code or title to filter"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    style={{ width: '100%', marginBottom: '48px', marginTop: '10px' }}
                                    className={classes.textField + " commonClass textLabelClass"}
                                    margin="normal"
                                />
                            </Grid>
                        </Grid>
                        <Grid container className={classes.root} spacing={24}>
                            <Grid item lg={3} md={12} sm={12} xs={12} style={{ paddingRight: '15.4px' }}>
                                <FormControl className={classes.formControl} style={{ width: '100%' }} className={classes.selectForm + " commonClass textLabelClass"}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder">
                                        Division <span className="astrick">*</span>
                                    </InputLabel>
                                    <NativeSelect
                                        input={<Input name="age" id="age-native-label-placeholder" />}
                                    >
                                        <option value="">990 (Division Label)</option>
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </NativeSelect>
                                </FormControl>
                            </Grid>
                            <Grid item lg={4} md={12} sm={12} xs={12} style={{ paddingRight: '12px' }}>
                                <FormControl className={classes.formControl} style={{ width: '100%' }} className={classes.selectForm + " commonClass textLabelClass"}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder">
                                        Department <span className="astrick-r">*</span>
                                    </InputLabel>
                                    <NativeSelect
                                        input={<Input name="age" id="age-native-label-placeholder" />}
                                    >
                                        <option value="">53 (Department Label)</option>
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </NativeSelect>
                                </FormControl>
                            </Grid>
                            <Grid item lg={5} md={12} sm={12} xs={12}>
                                <FormControl className={classes.formControl} style={{ width: '100%' }} className={classes.selectForm + " commonClass textLabelClass"}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder">
                                        Job Code <span className="astrick-r">*</span>
                                    </InputLabel>
                                    <NativeSelect
                                        input={<Input name="age" id="age-native-label-placeholder" />}
                                    >
                                        <option value="">101 (Department Manager)</option>
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </NativeSelect>
                                </FormControl>
                            </Grid>
                        </Grid>
                        <hr className={classes.sectionDesktop + " divider-hr"} />
                        <Typography variant="display2" style={{ marginBottom: '33px' }} gutterBottom className={classes.headerTitle1 + ' commonClass'}>
                            Requisition Details
                            </Typography>
                        <Grid container className={classes.gridContainer} spacing={24} >
                            <Grid item lg={3} md={12} sm={12} xs={12} style={{ paddingRight: '23.8px' }}>
                                <FormControl className={classes.formControl} style={{ width: '100%' }} className={classes.selectForm + " commonClass textLabelClass"}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder">
                                        Select Employment Type <span className="astrick-r">*</span>
                                    </InputLabel>
                                    <NativeSelect
                                        input={<Input name="age" id="age-native-label-placeholder" />}
                                    >
                                        <option value="">Part Time</option>
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </NativeSelect>
                                </FormControl>
                            </Grid>
                            <Grid item lg={9} md={12} sm={12} xs={12} style={{ paddingRight: '12px' }}>
                                <FormLabel className={classes.labelText + " commonClass"} style={{ width: '100%' }}>Select to see current applicants for the job code entered above.</FormLabel>
                                <Button className={classes.potentialButton + " commonClass"} color="inherit" onClick={this.openApplicantPool}>
                                    Potential Applicant Pool
                                    </Button>
                            </Grid>
                        </Grid>
                        <Grid container className={classes.root} spacing={24}>
                            <Grid item lg={3} md={12} sm={12} xs={12} style={{ paddingRight: '15.4px' }}>
                                <FormControl className={classes.formControl} style={{ width: '100%' }} className={classes.selectForm + " commonClass textLabelClass"}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder">
                                        Select Number of Positions <span className="astrick-r">*</span>
                                    </InputLabel>
                                    <NativeSelect
                                        input={<Input name="age" id="age-native-label-placeholder" />}
                                    >
                                        <option value="">1</option>
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </NativeSelect>
                                </FormControl>
                            </Grid>
                            <Grid item lg={4} md={12} sm={12} xs={12} style={{ paddingRight: '12px' }}>
                                <FormControl className={classes.formControl} style={{ width: '100%' }} className={classes.selectForm + " commonClass textLabelClass"}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder">
                                        Hiring Manager <span className="astrick-r">*</span>
                                    </InputLabel>
                                    <NativeSelect
                                        input={<Input name="age" id="age-native-label-placeholder" />}
                                    >
                                        <option value="">Jason Smith</option>
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </NativeSelect>
                                </FormControl>
                            </Grid>
                            <Grid item lg={5} md={12} sm={12} xs={12}>
                                <FormControl className={classes.formControl} style={{ width: '100%' }} className={" commonClass textLabelClass"}>
                                    <InputLabel shrink htmlFor="age-native-label-placeholder">
                                        Reason for Creation <span className="astrick-r">*</span>
                                    </InputLabel>
                                    <NativeSelect
                                        input={<Input name="age" id="age-native-label-placeholder" />}
                                    >
                                        <option value="">Jason Smith</option>
                                        <option value={10}>Ten</option>
                                        <option value={20}>Twenty</option>
                                        <option value={30}>Thirty</option>
                                    </NativeSelect>
                                </FormControl>
                                <Typography
                                    variant="title" gutterBottom className={classes.infoSelect + ' commonClass'}>
                                    2 similar positions have been opened in your store for this job code. Click here for more info.
                                    </Typography>
                            </Grid>
                            <div className={classes.sectionMobile}>
                                <div className={classes.cardHeader}>
                                    <CardMedia style={{ backgroundSize: 'contain' }}
                                        className={classes.cover}
                                        image="/images/information-circled-copy.png"
                                        title="Info"
                                    />
                                    <CardContent className={classes.content + " commonClass"}>
                                        <Typography className=" commonClass" style={{ fontSize: '16px', marginTop: '-20px' }}>New requisitions will be closed 30 days after creation date.</Typography>
                                    </CardContent>
                                </div>
                            </div>
                        </Grid>
                        <Grid container className={classes.root} spacing={24}>
                            <Grid item xs={12} style={{ paddingRight: '15.4px', paddingTop: '45px' }}>
                                <Button className={classes.dashboard + " commonClass btn-submit"} color="inherit" >
                                    Create
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item lg={3} md={4} sm={12} xs={12} style={{ paddingRight: '15.4px' }} className={classes.sectionDesktop}>
                        <MediaCard toggleApplicantPool={this.state.toggleApplicantPool} />
                    </Grid>
                </Grid>

            </div>
        );
    }

    openApplicantPool = () => {
        this.setState((prevState) => {
            return {
                toggleApplicantPool: !prevState.toggleApplicantPool
            }
        });
    }

}

MainAppBarBody.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(MainAppBarBody);
