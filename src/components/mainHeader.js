import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    headerTitle: {
        fontSize: '30px',
        color: '#041f41',
        [theme.breakpoints.down('sm')]: {
            fontWeight: 'bold',
            fontSize: '30px'
        },
    },
    dashboard: {
        fontSize: '18px',
        color: '#0071ce',
        float: 'right',
        textTransform: 'none'
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'block'
        },
    },
});

function MainAppBarHeader(props) {
    const { classes } = props;
    return (
        <div className={classes.root} style={{ padding: '30px 0 36px 0' }}>
            <Grid container style={{ padding: '0  24px' }}>
                <Grid item lg={9} md={8} sm={7} xs={12}>
                    <Typography variant="display2" gutterBottom className={classes.headerTitle + ' commonClass'}>
                        Create Requisition
                    </Typography>
                </Grid>
                <Grid item lg={3} md={4} sm={5} xs={6} className={classes.sectionDesktop}>
                    <Button className={classes.dashboard + " commonClass"} color="inherit" >
                        Back to Dashboard
                     </Button>
                </Grid>
            </Grid>
        </div>
    );
}
MainAppBarHeader.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(MainAppBarHeader);
